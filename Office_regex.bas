Attribute VB_Name = "Module1"
'Office-sovelluksen regular expression -hakufunktio
        
'Joona Saari, 1.3.2017
'Seuraava funktio, nimelt��n regex, etsii Excel-tiedostosta vapaasti m��ritellyn halutun 'merkkijonon. Esimerkkihaku voi olla tiedoston solut, jotka sis�lt�v�t s�hk�postiosoitteen.

'K�ytt�ohje
'1) Avaa Exceliss� Kehitysty�kaluista Visual Basic
'2) Ota k�ytt��n Microsoft VBScript Regular Expressions 5.5 -paketti (Tools >> references.. >> 'Valitse listasta)
'3) Valitse File >> Import File ja valitse t�m� .bas tiedosto tai vaihtoehtoisesti liit� oheinen koodi VBAProject:iin modulina (insert >> module)
'4) Kutsu toimintoa Excel-solussa esim. =regex(A1, "(\w)"), miss� A1 tilalle laitetaan haluttu teksti� 'sis�lt�v� solu ja (\w) tilalle jokin Regular Expressions -koodi (koodi (\w) etsii esim. solun ensimm�isen kirjaimen). Esimerkiksi jos solu A1 sis�lt�� tekstin Hyv�� p�iv��, palauttaa =regex(A1, "(\w)") kirjaimen H.

'M��ritell��n funktio nimelt� regex, joka ottaa seuraavat input -arvot: etsitt�v� solu, haettava merkkijono ja output-arvo.
Function regex(etsiSolu As String, haeMerk As String, Optional ByVal outputArvo As String = "$0") As Variant

'Asetetaan kirjaston VBScript_RegExp_55 sallimat RegExp -objektit, jotka tekev�t varsinaisen Regex-ty�n.
Dim RegExpInput As New VBScript_RegExp_55.RegExp, RegExpReplace As New VBScript_RegExp_55.RegExp

'M��ritell��n viel� yksi muuttuja.
Dim inputHaku As Object

'M��ritell��n RegExp -objektien ominaisuudet.
'1) Input-merkkijonoon teht�v�t RegExp-muutokset: input-arvosta etsit��n muuttujan haeMerk ilmaisema merkkijono.
With RegExpInput
        .Global = True
        .MultiLine = True
        .IgnoreCase = False
        .Pattern = haeMerk
    End With

'2. M��ritell��n, mill� arvolla input-arvo korvataan. Mik�li funktiota asetettaessa arvo outputArvo on $0, niin alla oleva objekti palauttaa kyseisen arvon. $0 merkitsee, ett� funktio regex palauttaa l�ytyneen merkkijonon haeMerk. Mik�li outputArvo ei ole $0, niin objekti palauttaa muuttujalle outputArvo t�ll�in asetetun muun arvon.
    With RegExpReplace
        .Global = True
        .MultiLine = True
        .IgnoreCase = False
       .Pattern = "\$0"
End With

'Jos RegExp etsint� ei tuottanut tulosta palauttaa funktio regex t�m�n avulla tyhj�n arvon.
Set inputHaku = RegExpInput.Execute(etsiSolu)
    If inputHaku.Count = 0 Then
        regex = ""

Else

' Jos etsint� tuottaa tuloksen, niin funktio regex palauttaa sen t�ss�.
                regex = RegExpReplace.Replace(outputArvo, inputHaku(0).Value)
    End If
End Function








